<?php

/**
 * @file
 *
 * Theme implementation: Template for each forum post whether node or comment.
 *
 * All variables available in node.tpl.php and comment.tpl.php for your theme
 * are available here. In addition, Advanced Forum makes available the following
 * variables:
 *
 * - $top_post: TRUE if we are formatting the main post (ie, not a comment)
 * - $reply_link: Text link / button to reply to topic.
 * - $total_posts: Number of posts in topic (not counting first post).
 * - $new_posts: Number of new posts in topic, and link to first new.
 * - $links_array: Unformatted array of links.
 * - $account: User object of the post author.
 * - $name: User name of post author.
 * - $author_pane: Entire contents of the Author Pane template.
 */

?>

<?php if ($top_post): ?>
  <?php print $topic_header ?>
<?php endif; ?>


<article id="<?php print $post_id; ?>" class="<?php print $classes; ?><?php print $top_post ? ' top-post' : 'reply-post'; ?>" <?php print $attributes; ?>>
  <div class="row">

    <div class="column large-2">
      <aside>

        <div class="photo">
          <div class="inner-photo"></div>
        </div>

      </aside>
    </div>

    <div class="column large-10">
      <section class="post-content">
        <header>

          <?php if (!empty($in_reply_to)): ?>
           <span class="forum-in-reply-to"><?php print $in_reply_to; ?></span>
          <?php endif; ?>
          <span class="forum-post-number"><?php print $permalink; ?></span>

          <?php if (!$node->status && $top_post): ?>
            <span class="label radius alert"><?php print t("Unpublished post") ?></span>
          <?php endif; ?>

          <?php if ($status === 'comment-unpublished' && !$top_post): ?>
            <span class="label radius alert"><?php print t("Awaiting approval") ?></span>
          <?php endif; ?>

          <?php if (!empty($author_pane)): ?>
            <?php print $author_pane; ?>
          <?php endif; ?>

          <?php print $date; ?>

          <div class="forum-posted-on">

            <?php
            // This whole section is for printing the "new" marker. With core comment
            // we just need to check a variable. With Node Comment, we need to do
            // extra work to keep the views caching used for Node Comment from
            //caching the new markers.
            ?>
            <?php if (!$top_post): ?>
              <?php if (!empty($new)): ?>
                <a id="new"><span class="new">(<?php print $new ?>)</span></a>
              <?php endif; ?>

              <?php if (!empty($first_new)): ?>
                <?php print $first_new; ?>
              <?php endif; ?>

              <?php if (!empty($new_output)): ?>
                <?php print $new_output; ?>
              <?php endif; ?>
            <?php endif; ?>
          </div>  <?php // End of posted on div ?>
        </header>
        <main>
          <?php if (!empty($title)) : ?>
          <h2 class="post-title"><?php print $title; ?></h2>
          <?php endif; ?>

          <div class="forum-post-content">
            <?php
              // We hide the comments and links now so that we can render them later.
              hide($content['taxonomy_forums']);
              hide($content['comments']);
              hide($content['links']);
              if (!$top_post)
                hide($content['body']);
              print render($content);
            ?>

            <?php if (!empty($post_edited)): ?>
              <div class="post-edited">
                <?php print $post_edited ?>
              </div>
            <?php endif; ?>

            <?php if (!empty($signature)): ?>
              <div class="author-signature">
                <?php print $signature ?>
              </div>
            <?php endif; ?>

          </div>

        </main>
        <footer>
          <div class="row">
            <div class="columns large-12">
              <div class="forum-post-links">
                <?php print render($content['links']); ?>
              </div>
            </div>
          </div>
        </footer>
      </section>
    </div>

  </div>
</article>

<?php print render($content['comments']); ?>
