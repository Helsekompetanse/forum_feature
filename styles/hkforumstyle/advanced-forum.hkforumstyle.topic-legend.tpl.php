<?php

/**
 * @file
 * Theme implementation to show forum legend.
 */
?>

<div class="forum-topic-legend clearfix">
  <div class="forum-topic-legend-item">
    <span class="topic-icon topic-icon-new"></span>
    <span class="forum-topic-legend-text"><?php print t('New posts'); ?></span>
  </div>
  <div class="forum-topic-legend-item">
    <span class="topic-icon topic-icon-default"></span>
    <span class="forum-topic-legend-text"><?php print t('No new posts'); ?></span>
  </div>
  <div class="forum-topic-legend-item">
    <span class="topic-icon topic-icon-hot-new"></span>
    <span class="forum-topic-legend-text"><?php print t('Hot topic with new posts'); ?></span>
  </div>
  <div class="forum-topic-legend-item">
    <span class="topic-icon topic-icon-hot"></span>
    <span class="forum-topic-legend-text"><?php print t('Hot topic without new posts'); ?></span>
  </div>
  <div class="forum-topic-legend-item">
    <span class="topic-icon topic-icon-sticky"></span>
    <span class="forum-topic-legend-text"><?php print t('Sticky topic'); ?></span>
  </div>
  <div class="forum-topic-legend-item">
    <span class="topic-icon topic-icon-closed"></span>
    <span class="forum-topic-legend-text"><?php print t('Locked topic'); ?></span>
  </div>
</div>
