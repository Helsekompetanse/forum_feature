<?php

/**
 * @file
 *
 * Theme implementation: Template for forum topic header.
 *
 * - $node: Node object.
 * - $search: Search box to search this topic (Requires Node Comments)
 * - $reply_link: Text link / button to reply to topic.
 * - $total_posts_count: Number of posts in topic.
 * - $new_posts_count: Number of new posts in topic.
 * - $first_new_post_link: Link to first unread post.
 * - $last_post_link: Link to last post.
 * - $pager: Topic pager.
 */
?>

<div class="row">

  <div class="columns large-9">
    <?php if (!empty($secondary_actions)) : ?>
    <?php print $secondary_actions; ?>
    <?php endif; ?>
  </div>
  <div class="columns large-3">
    <?php print $total_posts_count; ?> / <?php print t('!new new', array('!new' => $new_posts_count)); ?>
  </div>
</div>

<!-- <div id="forum-topic-header" class="forum-topic-header clearfix">
	<?php if (!empty($search)): ?>
	  <?php print $search; ?>
  <?php endif; ?>


  <a id="forum-topic-top"></a>
</div>
 -->
