<?php
/**
 * @file
 * hk_forum_feature.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function hk_forum_feature_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer advanced forum'.
  $permissions['administer advanced forum'] = array(
    'name' => 'administer advanced forum',
    'roles' => array(
      'Batman' => 'Batman',
      'Robin' => 'Robin',
    ),
    'module' => 'advanced_forum',
  );

  // Exported permission: 'administer forums'.
  $permissions['administer forums'] = array(
    'name' => 'administer forums',
    'roles' => array(
      'Batman' => 'Batman',
      'Robin' => 'Robin',
    ),
    'module' => 'forum',
  );

  // Exported permission: 'create forum content'.
  $permissions['create forum content'] = array(
    'name' => 'create forum content',
    'roles' => array(
      'Batman' => 'Batman',
      'Robin' => 'Robin',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any forum content'.
  $permissions['delete any forum content'] = array(
    'name' => 'delete any forum content',
    'roles' => array(
      'Batman' => 'Batman',
      'Robin' => 'Robin',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own forum content'.
  $permissions['delete own forum content'] = array(
    'name' => 'delete own forum content',
    'roles' => array(
      'Batman' => 'Batman',
      'Robin' => 'Robin',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete terms in forums'.
  $permissions['delete terms in forums'] = array(
    'name' => 'delete terms in forums',
    'roles' => array(
      'Batman' => 'Batman',
      'Robin' => 'Robin',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit any forum content'.
  $permissions['edit any forum content'] = array(
    'name' => 'edit any forum content',
    'roles' => array(
      'Batman' => 'Batman',
      'Robin' => 'Robin',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own forum content'.
  $permissions['edit own forum content'] = array(
    'name' => 'edit own forum content',
    'roles' => array(
      'Batman' => 'Batman',
      'Robin' => 'Robin',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit terms in forums'.
  $permissions['edit terms in forums'] = array(
    'name' => 'edit terms in forums',
    'roles' => array(
      'Batman' => 'Batman',
      'Robin' => 'Robin',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'view forum statistics'.
  $permissions['view forum statistics'] = array(
    'name' => 'view forum statistics',
    'roles' => array(
      'Batman' => 'Batman',
      'Robin' => 'Robin',
    ),
    'module' => 'advanced_forum',
  );

  // Exported permission: 'view last edited notice'.
  $permissions['view last edited notice'] = array(
    'name' => 'view last edited notice',
    'roles' => array(
      'Batman' => 'Batman',
      'Robin' => 'Robin',
    ),
    'module' => 'advanced_forum',
  );

  return $permissions;
}
